# Testing Script

Based on template from Steve Krug

### THE INSTRUCTIONS

Hi, ___________. My name is ___________, and I’m going to be walking you through this session today.

Before we begin, I have some information for you, and I’m going to read it to make sure that I cover everything.

You probably already have a good idea of why we asked you here, but let me go over it again briefly. We’re working on some potential updates to a website, and we want to test it with you so we can see whether it works as intended.

The session should take about 20 minutes.The first thing I want to make clear right away is that we’re testing the *site*, not you. You can’t do anything wrong here. In fact, this is probably the one place today where you don’t have to worry about making mistakes.

As you use the site, I’m going to ask you as much as possible to try to think out loud: to say what you’re looking at, what you’re trying to do, and what you’re thinking. This will be a big help to us.

Also, please don’t worry that you’re going to hurt our feelings. We recognize that the prototype we have created is very rough, and so you may have a lot of feedback for us. We want to hear all of it. It will greatly help us improve our design.

If you have any questions as we go along, just ask them. I may not be able to answer them right away, since we’re interested in how people do when they don’t have someone sitting next to them to help. But if you still have any questions when we’re done I’ll try to answer them then. And if you need to take a break at any point, just let me know.

You may have noticed the microphone. With your permission, we’re going to record what happens in our conversation. The recording will only be used to help us figure out how to improve the site, and it won’t be seen by anyone except the people working on this project. And it helps me, because I don’t have to take as many notes.

**Begin the audio recording.**

Do you have any questions so far?

### THE QUESTIONS

OK. Before we look at the site, I’d like to ask you just a few quick questions.

First, are you a student at BYU? What program are you in? How many semesters have you studied here?

Roughly how many hours a week altogether—just a rough estimate—would you say you spend using the Internet, including Web browsing and email, at work and at home?

What kinds of sites (work and personal) are you looking at when you browse the Web?

Do you have any favorite Web sites?

THE HOME PAGE TOUR

OK, great. We’re done with the questions, and we can start looking at things.

**Pull out the prototype. Hand them the front page.**

First, I’m going to ask you to look at this page and tell me what you make of it: 

* what strikes you about it
* whose site you think it is
* what you can do here
* what it’s for

 Just look around and do a little narrative.

You can scroll if you want to, but don’t click on anything yet.

**Allow this to continue for three or four minutes, at most.**

### THE TASKS

Thanks. Now I’m going to ask you to try doing some specific tasks.

I’m going to read each one out loud. I’m also going to ask you to do these tasks. We’ll learn a lot more about how well the site works that way.

And again, as much as possible, it will help us if you can try to think out loud as you go along.

**Hand the participant the first scenario, and read it aloud.**

**Allow the user to proceed until you don’t feel like it’s producing any value or the user becomes very frustrated.Repeat for each task or until time runs out.**

### PROBING

Thanks, that was very helpful.
**Probe anything you want to follow up on.**

### WRAPPING UP

Do you have any questions for me, now that we’re done?

**Stop the audio recording and save the file.**

Thank them and escort them out.

### TASK 1

**Imagine you’re a first-year freshman who has declared their major to be Geology. You’re signing up for your first semester of classes. You’ve already signed up for the following classes, totaling 13.0 credits:**

* American Heritage 100 (3.0 credits)

* CIV 201 (3.0 credits)

* Teachings of the Book of Mormon (2.0 credits)

* Interior Design (3.0 credits)

* Missionary Preparation (2.0 credits)

You want to add a fun class to your schedule, but you also don’t want to take any more than 15.0 credits this semester.

### TASK 2

**Now imagine you’re still the Geology major student but now you’re a senior, planning out your last semester before graduation. You’ve suddenly realized that you haven’t fulfilled all your General Education requirements: you still need a class for your Physical Sciences credit.**

Use the Elective Finder tool to help you find a class to fulfill it.

**How to run Task 1**

Task 1 is exactly what we did in class. The best answer is the SWELL class.

**How to run Task 2**

* The right answer is the PHYSCS 121 class.

* We expect them to use the filters tools.

* If they get the wrong answer, let them know.

* After the task is over, perhaps emphasize the fact that we’re testing the site, not them.

* If they get the right answer, don’t say anything.

