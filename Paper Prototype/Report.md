# Prototype - Key Features

## Entry Point: existing class registration page

![prototype0](./prototype0.jpg)

### Guiding Usability Principles

* **Learnability & Memorability**:
  * users already familiar with the existing registration process don't need to learn how to use a new page in order to interact with this feature
* **Consistency**:
  * this feature integrates with the existing style & structure of myMap to make for a cohesive feel
* **Efficiency**
  * this feature can be accessed mid-workflow as students are actively selecting classes to register for, they don't need to use a separate tool and/or navigate between different views to use it

## Explorer Modal & Course Cards

![prototype1](./prototype1.jpg)

### Guiding Usability Principles

* **Learnability & Memorability**
  * card-swiping is a very common design paradigm that university students are likely to have encountered before in other apps & websites
  * to students unfamiliar with this paradigm, large icons make for an intuitive & learnable experience
* **Efficiency**
  * students are able to gain exposure to a high volume of potential courses without being overloaded with information
  * users can then spend more time reading details about courses that are good matches
  * users can also immediately redirect to register for a course if they find a good match
* **Satisfaction**
  * users are likely to draw parallels between this feature & various dating sites. Makes for an enjoyable & whimsical (while still practical) interactive experience

## The Liked Tab

![prototype2](./prototype2.jpg)

### Guiding Usability Principles

* **Efficiency**
  * allow users to quickly see courses they've selected as candidates
  * allow users to quickly register for candidate courses
* **Error Tolerance/Prevention**
  * allow users to remove accidentally liked courses or courses they are no longer interested in
  * easily navigate to/from the liked tab & the explore modal

## Filters Drawer

![prototype3](./prototype3.jpg)

### Guiding Usability Principles

* **Efficiency & Satisfaction**
  * enable edits without navigating away from the explore modal main view
  * enable fine-tuned searching to maximize relevant courses displayed & minimize time spent browsing

## Exit Point: Existing Registration Page (w/ search pre-populated)

![prototype4](./prototype4.jpg)

### Guiding Usability Principles

* **Efficiency**
  * prevent need for user to redundantly/manually search for courses they found in the elective explorer
  * return user to the normal registration workflow without any redirects or manual navigation
* **Learnability**
  * registration as part of the elective discovery workflow is no different than normal registration, so the user doesn't need to learn multiple ways to register
* **Error Tolerance & Prevention**
  * allow user to look over course in full detail on the existing registration page while selecting sections before actually registering for a course

# Testing Script

Based on template from Steve Krug

### THE INSTRUCTIONS

Hi, ___________. My name is ___________, and I’m going to be walking you through this session today.

Before we begin, I have some information for you, and I’m going to read it to make sure that I cover everything.

You probably already have a good idea of why we asked you here, but let me go over it again briefly. We’re working on some potential updates to a website, and we want to test it with you so we can see whether it works as intended.

The session should take about 20 minutes.The first thing I want to make clear right away is that we’re testing the *site*, not you. You can’t do anything wrong here. In fact, this is probably the one place today where you don’t have to worry about making mistakes.

As you use the site, I’m going to ask you as much as possible to try to think out loud: to say what you’re looking at, what you’re trying to do, and what you’re thinking. This will be a big help to us.

Also, please don’t worry that you’re going to hurt our feelings. We recognize that the prototype we have created is very rough, and so you may have a lot of feedback for us. We want to hear all of it. It will greatly help us improve our design.

If you have any questions as we go along, just ask them. I may not be able to answer them right away, since we’re interested in how people do when they don’t have someone sitting next to them to help. But if you still have any questions when we’re done I’ll try to answer them then. And if you need to take a break at any point, just let me know.

You may have noticed the microphone. With your permission, we’re going to record what happens in our conversation. The recording will only be used to help us figure out how to improve the site, and it won’t be seen by anyone except the people working on this project. And it helps me, because I don’t have to take as many notes.

**Begin the audio recording.**

Do you have any questions so far?

### THE QUESTIONS

OK. Before we look at the site, I’d like to ask you just a few quick questions.

First, are you a student at BYU? What program are you in? How many semesters have you studied here?

Roughly how many hours a week altogether—just a rough estimate—would you say you spend using the Internet, including Web browsing and email, at work and at home?

What kinds of sites (work and personal) are you looking at when you browse the Web?

Do you have any favorite Web sites?

THE HOME PAGE TOUR

OK, great. We’re done with the questions, and we can start looking at things.

**Pull out the prototype. Hand them the front page.**

First, I’m going to ask you to look at this page and tell me what you make of it: 

* what strikes you about it
* whose site you think it is
* what you can do here
* what it’s for

 Just look around and do a little narrative.

You can scroll if you want to, but don’t click on anything yet.

**Allow this to continue for three or four minutes, at most.**

### THE TASKS

Thanks. Now I’m going to ask you to try doing some specific tasks.

I’m going to read each one out loud. I’m also going to ask you to do these tasks. We’ll learn a lot more about how well the site works that way.

And again, as much as possible, it will help us if you can try to think out loud as you go along.

**Hand the participant the first scenario, and read it aloud.**

**Allow the user to proceed until you don’t feel like it’s producing any value or the user becomes very frustrated.Repeat for each task or until time runs out.**

### PROBING

Thanks, that was very helpful.
**Probe anything you want to follow up on.**

### WRAPPING UP

Do you have any questions for me, now that we’re done?

**Stop the audio recording and save the file.**

Thank them and escort them out.

### TASK 1

**Imagine you’re a first-year freshman who has declared their major to be Geology. You’re signing up for your first semester of classes. You’ve already signed up for the following classes, totaling 13.0 credits:**

* American Heritage 100 (3.0 credits)

* CIV 201 (3.0 credits)

* Teachings of the Book of Mormon (2.0 credits)

* Interior Design (3.0 credits)

* Missionary Preparation (2.0 credits)

You want to add a fun class to your schedule, but you also don’t want to take any more than 15.0 credits this semester.

### TASK 2

**Now imagine you’re still the Geology major student but now you’re a senior, planning out your last semester before graduation. You’ve suddenly realized that you haven’t fulfilled all your General Education requirements: you still need a class for your Physical Sciences credit.**

Use the Elective Finder tool to help you find a class to fulfill it.

**How to run Task 1**

Task 1 is exactly what we did in class. The best answer is the SWELL class.

**How to run Task 2**

* The right answer is the PHYSCS 121 class.

* We expect them to use the filters tools.

* If they get the wrong answer, let them know.

* After the task is over, perhaps emphasize the fact that we’re testing the site, not them.

* If they get the right answer, don’t say anything.

# Results & Analysis

### Common Pain Points & Pitfalls

* Entry Point:
  * ~40% of our testing participants struggled to find the "find electives" button on the course registration page
    * some of our participants were UVU students unfamiliar with the site (a way to simulate how a BYU true freshman would interact with the site)
    * some of the difficulty finding the button could be due to the low contrast & fidelity of the prototype and/or student's unfamiliarity with the existing site
    * also possible: the entry point is difficult to locate and may be better placed somewhere else in the site or displayed more prominently
  * students specifically looking for a "fun" class gravitated towards the "find electives" button, but when looking for a specific class or requirement, it wasn't always immediately clear that the "find electives" feature could fulfill this purpose
* Error tolerance
  * no easy "back" button for accidentally not-liking a course
* Catalog:
  * students consulted and cross-referenced the catalog more frequently that anticipated
    * may need to revisit what course information is present in the card view
* Filters:
  * students didn't make as heavy use of the filter criteria as expected
    * could be because the tasks we gave them weren't specific enough to warrant their use
    * may need to test with more tasks to ensure their effectiveness
* Exit Point
  * some participants were confused by the fact that the "register" button on the Explore Modal didn't actually register them for the class.
    * may need to rename the register button to make this more clear

### Successes

* the card-swiping paradigm was very intuitive and needed little to no explanation or fumbling around to understand
* once students entered the exploring modal, they generally knew what to do in order to carry out their task
* students enjoyed using the exploring modal, especially when they weren't really sure what to be looking for