# Figma Final Prototype — Testing Script

## Instructions/Disclaimer

Hi, **___**. My name is **___**, and I’m going to be walking you through this session today.

Before we begin, I have some information for you, and I’m going to read it to make sure that I cover everything.

You probably already have a good idea of why we asked you here, but let me go over it again briefly. We’re working on some potential updates to a website, and we want to test it with you so we can see whether it works as intended.

The session should take about 20 minutes.The first thing I want to make clear right away is that we’re testing the *site*, not you. You can’t do anything wrong here. In fact, this is probably the one place today where you don’t have to worry about making mistakes.

As you use the site, I’m going to ask you as much as possible to try to think out loud: to say what you’re looking at, what you’re trying to do, and what you’re thinking. This will be a big help to us.

Also, please don’t worry that you’re going to hurt our feelings. We recognize that the prototype we have created is very rough, and so you may have a lot of feedback for us. We want to hear all of it. It will greatly help us improve our design.

If you have any questions as we go along, just ask them. I may not be able to answer them right away, since we’re interested in how people do when they don’t have someone sitting next to them to help. But if you still have any questions when we’re done I’ll try to answer them then. And if you need to take a break at any point, just let me know.

You may have noticed the microphone. With your permission, we’re going to record what happens in our conversation. The recording will only be used to help us figure out how to improve the site, and it won’t be seen by anyone except the people working on this project. And it helps me, because I don’t have to take as many notes.

**Begin the audio recording.**

Do you have any questions so far?

## Initial Questions/Impressions

#### Questions/Demographics

* Are you a student at BYU?
* What program are you in?
* How many semesters have you studied here?
* How familiar are you with myMap? How frequently would you say you use it?

#### Home/Registration Page Tour

**Start the Prototype on Flow 1, displaying the registration page**

First, I’m going to ask you to look at this page and tell me what you make of it: 

* What strikes you about it?
* What you can do here?
* (If they have prior experience with myMap) Do you notice anything new or different about this page?

 Just look around and do a little narrative.

You can scroll if you want to, but don’t click on anything yet.

**Allow this to continue for 2-3 minutes, at most.**

## Procedures

**Now, we're going to give you a series of scenarios and tasks to perform. As you work through each scenario, be sure to think out loud, telling us everything you notice and what you expect to happen when you interact with the prototype. If you have any questions at any point, feel free to ask. We may not immediately answer every question you have, but we're happy to clarify anything that doesn't make sense**

* For each task, give the participant the initial prompt, then make note of how they perform with respect to each task's associated metrics. You may need to add additional segues between tasks if they end up choosing different courses than the ones we expected (e.g., "Ok, let's pretend that you selected these classes...")

## Tasks

### Flow 1 Phase 1 — Entry Point

**Imagine you’re a first-year freshman who is planning their second semester of classes. You haven't declared your major yet, but you're leaning towards Geology. You’ve already signed up for the following classes, totaling 13.0 credits:**

* American Heritage 100 (3.0 credits)

* CIV 201 (3.0 credits)

* Teachings of the Book of Mormon (2.0 credits)

* GEOL 101 (3.0 credits)

* Missionary Preparation (2.0 credits)

You want to add a fun class to your schedule, but you also don’t want to take more than 15.0 credits this semester. You're not really sure what kind of class you want to take.



*Test subject should be able to find the "Explore/Suggest Electives" button and enter the explorer modal*

Metrics to note:

* Roughly how much time did it take for subject to find the button?

* Was the button the first thing they tried, or did they try other features first?

  

### Flow 1 Phase 2 — Exploring

**Ok, now let's find a few classes that look like they might be a good fit. Remember that you're looking for a fun class to take in addition to the classes you've already registered for**

*Test subject should add a combination of 1-3 SWELL and art classes to their "Liked" list. None of them should be more than 3.0 credit hours*

*The first time the user clicks "like" on a course, stop them and ask them what they think clicking "like" does*

Metrics to note:

* How long did it take to add these classes to their list?
* How often (if at all) did they navigate away from the card-swiping modal (i.e., return to registration page, catalog, etc.)?
* Did the subject generally have a good feel for how to add a class to their liked list or discard a class from their suggestions?

### Flow 1 Phase 3 — Committing

**Now that you've found a few classes that seem promising, let's choose one to register for and add it to your schedule. Let's say you just remembered that a close friend is currently in a SWELL hockey class this semester and told you that the tests are hard and it isn't really worth the work**

*Ideally, the test subject should access the "Liked" tab, remove SWELL hockey from their liked list, and click "Register" on one of the other courses. They don't necessarily need to remove hockey from their liked list to successfully complete the task, but they shouldn't register for it*

**NOTE: ensure the user understands that when they return to the registration page, they still need to select and add a section to their schedule**

### Flow 2 Phase 1 — Filters

*Return to the explorer modal*

**Ok, now imagine that you are a junior who has finished all of their GE requirements except for Arts. You have a pretty busy school and work schedule this semester and you'd ideally like to find an art class that has an online section available. Let's look for a couple of classes that might work**

*The test subject should open the filters ribbon tab and apply the "online available" and "fulfills GE" filters. Then they should find and like/register for 1-2 classes that satisfy the task parameters*

*If after a while they don't find/use the filters, prompt them by saying:* **You have a decent idea of what kind of classes you want to take, so you don't want to just randomly swipe through all of the courses. Try to narrow down your search**

Metrics:

* were they able to find the filters tab without being prompted?
* did the filters they applied match what we told them to look for?
* did the classes they added fit the parameters you gave them?

## Probing & Wrapping Up

Thanks, that was very helpful.

* What parts of this session did you have the hardest time understanding?
* Were there any times you expected something to happen but something else happened instead?
* How would you compare this test run to your normal class registration flow?
* Were there parts of this session that felt tedious or repetitive to you?

**Probe anything you want to follow up on.**

* Do you have any questions for me, now that we’re done?

**Stop the audio recording and save the file.**

Thank them and escort them out.