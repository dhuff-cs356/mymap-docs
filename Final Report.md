# MyMap Prototype: Electives Explorer

Zachary Allen, Braeden Hintze, Dallin Huff

[toc]

## Final Prototype Link

[Figma Link](https://www.figma.com/proto/2AcWzi0ARlWKJt2yNBkGiA/MyMAP-Discovery-Prototype?node-id=1%3A2&starting-point-node-id=1%3A2&scaling=scale-down-width)

## Feature Tour

 [Video Link](./feature tour.mov)

## Problem Statement

MyMap's current implementation makes the assumption that by the time students arrive at the registration page, they already know what class(es) they are going to register for. Consequently, students struggle to discover and register for elective/optional courses that fit their schedules and interests in a timely manner without considerable outside research and cross-site navigation.

## Why Our Solution Works

The results from our tests indicate that the Electives Explorer provides an intuitive and efficient means for students to gain exposure to a high volume of elective courses that fit their schedules and interests without adding undue overhead to their existing registration workflow or introducing the need to consult numerous outside resources for additional information.

## Paper Prototype — Summary & Analysis

#### Guiding Design Principles & Patterns

* Efficiency
  * This feature can be accessed mid-workflow as students are actively selecting classes to register for, they don't need to use a separate tool and/or navigate between different views to use it
  * Students are able to gain exposure to a high volume of potential courses without being overloaded with information
  * Prevents/minimizes the need for user to redundantly/manually search for courses that they found in the elective explorer again on the registration page
* Learnability
  * Users already familiar with the existing registration process don't need to learn how to use a new page in order to interact with this feature
  * Card-swiping is a very common design paradigm that university students are likely to have encountered before in other apps & websites
  * To students unfamiliar with this pattern, large icons make for an intuitive & learnable experience
* Satisfaction
  * Design analogous to a "dating site" to maximize entertainment & user enjoyment without sacrificing practicality. Users gain exposure to a high volume of classes that they may have otherwise been unaware of and are easily able to add them to their schedule.

#### Testing Procedures

* Link: [Testing Script](./Paper Prototype/Testing Script.md)

#### Analysis/Findings from Tests

##### Strengths

* For our test subjects, the card-swiping paradigm was very intuitive and needed little to no explanation or fumbling around to understand. Users were generally aware of what a "Like" entailed
* Once students entered the explore modal, they generally knew what to do in order to carry out their task.
* Students enjoyed using the explore modal, especially when they weren't really sure what to be looking for

##### Weaknesses & Pain Points

* Entry Point:
  * ~40% of our testing participants struggled to find the "find electives" button on the course registration page
    * some of our participants were UVU students unfamiliar with the site (a way to simulate how a BYU true freshman would interact with the site)
    * some of the difficulty finding the button could be due to the low contrast & fidelity of the prototype and/or student's unfamiliarity with the existing site
    * also possible: the entry point is difficult to locate and may be better placed somewhere else in the site or displayed more prominently
  * students specifically looking for a "fun" class gravitated towards the "find electives" button, but when looking for a specific class or requirement, it wasn't always immediately clear that the "find electives" feature could fulfill this purpose
* Error tolerance
  * no easy "back" button for accidentally discarding a course
* Catalog:
  * students consulted and cross-referenced the catalog more frequently that anticipated
* Filters:
  * students didn't make as heavy use of the filter criteria as expected
    * could be because the tasks we gave them weren't specific enough to warrant their use
* Exit Point
  * some participants were confused by the fact that the "register" button on the Explore Modal didn't actually register them for the class

##### Original Report

Link: [Report](./Paper Prototype/report.md)

## First Digital Prototype — Summary & Analysis

#### Changes made from Paper Prototype

* Changed the "filters" and "liked" buttons to be tabs on a ribbon that expands over the modal instead of taking the user to a separate view
  * Guiding Principle: **error prevention/tolerance** — avoids ambiguous scenarios where a user may exit the filters tab and be unsure if their changes were saved/discarded
  * Pattern: tabbed group/ribbon
  * Rationale: make it more visually apparent when and to what the filters are applied
* Made the list of liked courses a horizontal-scrollable list of cards with condensed information to be more consistent with the card view from inside the card-swiping view
  * Guiding Principle: **consistency** — harmonizing views that model similar information to appear visually similar
* Added icons to text buttons to better communicate the action that each button does
  * Guiding Principle: **learnability** — visually communicate what each element of the product does to those who are unfamiliar with it

#### Notable changes NOT made

* Kept the name & location of the "Find Electives" button to see if previous subjects' difficulties in finding the button were simply due to the paper prototype's low fidelity

#### Testing Procedures

* Link: [Testing Script](./Paper Prototype/Testing Script.md)

#### Analysis/Findings from Tests

##### Strengths

* Having higher fidelity course cards with specific/pertinent information about each card greatly reduced the need for students to consult the course catalog page for most courses
* Higher fidelity views of the register button

##### Weaknesses

* The higher fidelity prototype helped new users to find the "Find Electives" button, but people with previous experience on MyMAP often overlooked the button; immediately starting their normal workflow.
* Some test subjects took longer than necessary to move from the card-swiping portion of the task to the liked courses/registration portion of the task. Users likely need some sort of prompt/indicator/cue to visit their liked courses tab
* The horizontal-scrolling liked courses list was significantly more difficult to use than anticipated. Though the cards were
* Overall lack of visual consistency and continuity — buttons are too large, backdrop colors contrast too much from existing pages, ribbon tabs aren't immediately recognized by some users as being clickable, etc.

##### Other Notes

* The extremely high number of permutations in paths and choices users can make made tests very difficult to conduct because our prototype flows couldn't account for everything subjects could do. This is a limitation of our prototype that will better be remedied by refining and changing our testing procedures/tasks than by completely overhauling the prototype with live/responsive data

##### Original Report

Link: [Report](https://docs.google.com/document/u/0/d/1xaV5-al0g4eeqjV0tH2DAcS4ijKaXfy2iVtmUYXM6qY/mobilebasic)

## Final Prototype — Summary & Analysis

#### Changes Made from Preliminary Prototype

* Revamped testing script to gain more quantitative insights on a task's successful completion and to help compartmentalize different phases of the workflow into smaller tasks
* Revamped visuals & colors to improve **consistency**/coherence with larger myMap ecosystem
* Made the liked courses tab a vertical scrollable list as a response to user feedback in previous tests
  * **satisfaction**, **efficiency**
* Added more functionality to the liked list: 
  * ability to "un-like" a course: **deferred decisions, error tolerance/prevention**
  * drag indicators to change sort order

* Separated each filter checkbox by line to improve readability
  * **learnability**, **satisfaction**
* Renamed "Find Electives" button to "Explore Electives" to better communicate the action's idiomatic purpose
  * **learnability**, **memorability**, **error tolerance/prevention**
* Added a badge indicator on the liked courses tab to help users keep track at a glance of how many courses they've liked without having to click on the tab
  * **efficiency**
* Added a back button for when users accidentally discard a course they wanted to like and a button to escape from the Explore modal
  * **error tolerance/prevention**

#### Notable Changes NOT Made

* We opted to forgo the "tutorial" landing page we had originally planned to implement after our second user study because we felt that the other design changes we made dramatically improved the overall intuitive nature of the design. We reworked our [testing script](./Final Script.md) to include a set of key metrics to monitor during each task in order to corroborate our design decisions in order to test our assumptions

#### Testing Procedures

Link: [Final Script](./Final Script.md)

#### Analysis/Findings from Tests

##### Strengths & Evidences/Metrics

* All users tested during our last round of tests described their experience as some combination of:
  * "familiar"

  * "intuitive"

  * "obvious"

  * "clear"

* 100% of tested users made 0 click errors in completing Flow 1 Task 1 and Flow 1 Task 2
* Nearly all click errors made in subsequent tasks had more to do with the order of actions than the actions themselves
  * e.g., a user clicked on the "open registration" button on an item in the liked list before first removing a class from the list as the task indicated

* Users took note of the badge indicator on the liked tab and were more likely to visit it than in previous prototype iterations/tests
* Feature improvements in this iteration helped to minimize the need for an "undo" or "escape" button (but having them just in case was useful in 1 or more instances)
* Other positive indicative comments from test subjects
  * “I didn’t even know these classes existed”
    * good indicator that our product is more capable of exposing students to courses they hadn't previously considered than the existing flow

  * "Polynesian Dance sounds awesome"
  * "This would be so useful in real life"
  * "Everyone knows what heart and x mean"


##### Limitations, Shortcomings & Future Changes to Explore

* Users were able to effectively use the filters we provided in the prototype, but some of them commented about additional filters or features that they would have liked — such as the ability to filter electives by category/teaching area
* The clickable features on the course cards didn't prove to be very useful for the specific tasks we gave our users. None of them clicked on the "view in catalog" or "register now" buttons during their workflows
  * This may be because our tasks didn't mandate their use, but it may be the case that most use cases don't mandate their use either

* The task assigned to the user in Flow 2 of our testing script was intentionally designed to give the user an edge-case scenario where they may or may not gravitate towards our feature to solve their problem. Consequently, one of our test subjects first tried to use the schedule builder feature instead of the elective finder.
  * This doesn't necessarily count as as a strike against our product's ability to solve our main problem statement, but it does indicate that students may consider other tools to be more valuable for problems outside of our scope.

  * With that being said, users still were able to effectively & efficiently complete the task with the elective finder, so its usefulness may lend itself to other problems outside of the scope of our focus
