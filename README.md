# README

This repository contains the user test analyses and revision notes for each phase of Zachary Allen, Braeden Hintze, and Dallin Huff's CS356 MyMap Project.

## Quick Links

[Final Prototype](https://www.figma.com/proto/2AcWzi0ARlWKJt2yNBkGiA/MyMAP-Discovery-Prototype?node-id=1%3A2&starting-point-node-id=1%3A2&scaling=scale-down-width)

[Prototype Walkthrough Video](./feature tour.mov)



[Final Writeup/Analysis](./Final Report.md)

[Final Test Script](./Final Script.md)



[Preliminary Digital Prototype Writeup](https://docs.google.com/document/u/0/d/1xaV5-al0g4eeqjV0tH2DAcS4ijKaXfy2iVtmUYXM6qY/mobilebasic)

[Preliminary Digital Prototype Test Script](./Paper Prototype/Test Script.md)



[Paper Prototype Writeup](./Paper Prototype/report.md)

[Paper Prototype Test Script](./Paper Prototype/Test Script.md)

